# git-sensible

Defaults most gitters can agree on.

Currently encompasses:

* Universally agreeable aliases
* A way to separate local configuration (eg. `user.name` and `user.email`)

Inspired by tpope's [vim-sensible][].

## Installation

    git clone https://gitlab.com/sorsasampo/git-sensible.git
    mv ~/.gitconfig ~/.gitconfig_local
    ln -rs git-sensible/.gitconfig ~/.gitconfig

You may find `git config -l --show-origin` useful for checking where options
are set.

## Features

See the [.gitconfig](.gitconfig).

Also refer to [git config --help][git-config].

[git-config]: https://git-scm.com/docs/git-config
[vim-sensible]: https://github.com/tpope/vim-sensible/
