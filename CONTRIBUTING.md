## Signoff

Contributions to this project must be signed off using `git commit --signoff`
to certify:

1. Agreement to [Developer's Certificate of Origin v1.1][DCO].
2. Agreement to submit the commit under [Apache License v2][ALv2] or any later
   version at the discretion of the primary author.

[ALv2]: https://www.apache.org/licenses/LICENSE-2.0
[DCO]: https://developercertificate.org/
